<?php

include_once(dirname(__FILE__) . '/CronJson.php');

class CronJsonError extends CronJson
{
    /**
     * @var string
     */
    public $title;

    /**
     * @var int
     */
    public $code;

    /**
     * @var string
     */
    public $detail;

    /**
     * @param array|null $data = null
     */
    public function __construct($data = null)
    {
        $this->setDataProperty($data, 'title', '');
        $this->setDataProperty($data, 'code', 0);
        $this->setDataProperty($data, 'detail', '');
    }
}

