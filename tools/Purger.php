<?php

include_once(dirname(__FILE__) . '/../../config/config.inc.php');
include_once(dirname(__FILE__) . '/../../init.php');
include_once(dirname(__FILE__) . '/CronJson/CronJsonResponse.php');
include_once(dirname(__FILE__) . '/CronJson/CronJsonError.php');

class Purger
{
    public static function purge($all = 0)
    {
        $cronJsonResponse = new CronJsonResponse();
        $cronJsonResponse->executed = true;
        $params['from'] = 'LS Purge Module';
        if ($all === 1) {
            $params['ALL'] = 1;
        } else {
            $params['public'] = ['*'];
        }
        
        Hook::exec('litespeedCachePurge', $params);
        $cronJsonResponse->successful = true;

        return $cronJsonResponse;
    }
}
