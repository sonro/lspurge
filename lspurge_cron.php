<?php

chdir(dirname(__FILE__));
include_once(dirname(__FILE__) . '/../../config/config.inc.php');
include_once(dirname(__FILE__) . '/../../init.php');
include_once(dirname(__FILE__) . '/CronJson/CronJsonResponse.php');
include_once(dirname(__FILE__) . '/CronJson/CronJsonError.php');
include_once(dirname(__FILE__) . '/tools/Purger.php');

$cronJsonResponse = new CronJsonResponse();

$all = Tools::getValue('all', 0);
$purge = Tools::getValue('purge', 0);

if (substr(_COOKIE_KEY_, 34, 8) != Tools::getValue('token')) {
    $cronJsonResponse->errors[] = new CronJsonError([
        'title' => 'LsPurge: Invalid token',
        'code' => 1,
        'detail' => 'Incorrect token supplied to LsPurge cron job',
    ]);
} else if ($_SERVER['REQUEST_METHOD'] !== 'GET') {
    $cronJsonResponse->errors[] = new CronJsonError([
        'title' => 'LsPurge: Not a GET request',
        'code' => 2,
        'detail' => 'LsPurge must be a GET request',
    ]);
} else if (!$purge) {
    $cronJsonResponse->errors[] = new CronJsonError([
        'title' => 'LsPurge: Invalid request',
        'code' => 3,
        'detail' => 'Must explicitly set `purge` value',
    ]);
} else {
    $cronJsonResponse = Purger::purge($all);
}

header('Content-Type: application/json');
echo $cronJsonResponse->toJson();
