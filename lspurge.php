<?php
if (!defined('_PS_VERSION_')) {
      exit;
}

class LsPurge extends Module
{
    public function __construct()
    {
        $this->name = 'lspurge';
        $this->tab = 'quick_bulk_update';
        $this->version = '0.1';
        $this->author = 'Christopher Morton';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_); 
        $this->bootstrap = true;
        $this->path = __PS_BASE_URI__.'modules/lspurge/';

        parent::__construct();

        $this->displayName = $this->l('LS Purge');
        $this->description = $this->l('Purge LiteSpeed cache for this shop or all shops via command or webapi');

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');
    }

    public function install()
    {
        if (Shop::isFeatureActive()) {
            Shop::setContext(Shop::CONTEXT_ALL);
        }

        if (parent::install()) {
            return true;
        }

        return false;
    }

    public function uninstall()
    {
        if (parent::uninstall()) {
            return true;
        }

        return false;
    }

    public function getContent()
    {
        $output = '';

        $cron_link = Tools::getHttpHost(true)
            .__PS_BASE_URI__
            .'modules/lspurge/lspurge_cron.php?purge=1&token='
            .substr(_COOKIE_KEY_, 34, 8);

        $this->context->smarty->assign(array(
            'cron_link' => $cron_link,
        ));

        $output .= $this->display(__FILE__, 'getContent.tpl');

        return $output;
    }

}
